Rails.application.routes.draw do
  resources :yourtours

  resources :tourisms

  resources :ats

  resources :addtravels

  resources :cultures

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  root 'articles#index'
  post '/aaa' => 'profiles#aaa'
  post '/live' => 'wildlives#live'
  post '/authenticate' => 'articles#authenticate'
  post '/almora' => 'birds#almora'
  post '/au' => 'registers#au'
  post '/abc' => 'cycles#abc'
  post '/authh' => 'trekkings#authh'
  post '/contactus' => 'articles#contactus'
  get 'articles/home' =>'articles#home'
  get 'articles/logout' =>'articles#logout',:as=>"logout"
  get 'cultures/index' =>'cultures#index'
  get 'articles/contact' => 'articles#contact', :as=>"contact"
  get 'registers/index' =>'registers#index'
  get 'birds/index' =>'birds#index'
  get 'abouts/index' =>'abouts#index'
  get 'wildlives/index' =>'wildlives#index'
  get 'birds/show' =>'birds#show'
  get 'hotels/index' =>'hotels#index'
  get 'trekkings/index' => 'trekkings#index'
  get 'birds/trekkings/index' => 'trekkings#index'
  get 'birds/trekkings/birds/show' => 'birds#show'
  get 'profiles/index' =>'profiles#index'
  get 'maps/index' =>'maps#index'
  get 'abouts/ae' =>'abouts#ae'
  post '/ae' => 'articles#ae'
  get 'cycles/index' => 'cycles#index'

 end
