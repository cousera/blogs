# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140802073832) do

  create_table "abouts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "addtravels", force: true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "tourdesc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "admins", force: true do |t|
    t.string   "email"
    t.string   "password"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "articles", force: true do |t|
    t.string   "email"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ats", force: true do |t|
    t.string   "email"
    t.string   "trip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "birds", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "tourname"
    t.datetime "checkin"
    t.datetime "checkout"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cultures", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cycles", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotels", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "maps", force: true do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profiles", force: true do |t|
    t.integer  "article_id"
    t.string   "name"
    t.integer  "age"
    t.string   "desc"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "registers", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "city"
    t.datetime "checkin"
    t.datetime "checkout"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tourisms", force: true do |t|
    t.string   "tour"
    t.string   "desc"
    t.string   "agegroup"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trekkings", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "tourname"
    t.datetime "checkin"
    t.datetime "checkout"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.integer  "contact"
    t.string   "gender"
    t.string   "email"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wildlives", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "yourtours", force: true do |t|
    t.string   "tour"
    t.string   "description"
    t.string   "agegroup"
    t.integer  "cost"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
