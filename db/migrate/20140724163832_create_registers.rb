class CreateRegisters < ActiveRecord::Migration
  def change
    create_table :registers do |t|
    	t.string :name
    	t.string :email
    	t.string :city
    	t.datetime :checkin
    	t.datetime :checkout
      t.timestamps
    end
  end
end
