class CreateBirds < ActiveRecord::Migration
  def change
    create_table :birds do |t|
    	t.string :name
    	t.string :email
    	t.string :tourname
    	t.datetime :checkin
    	t.datetime :checkout
      t.timestamps
    end
  end
end
