class CreateCultures < ActiveRecord::Migration
  def change
    create_table :cultures do |t|
    	t.string :firstname
    	t.string :lastname
    	t.string :email
    	t.string :tripdesc
    	t.number :cost
    	t.number :agegroup
    	t.string :season    	
      t.timestamps
    end
  end
end
