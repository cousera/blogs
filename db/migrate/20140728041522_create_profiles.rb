class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :article_id
      t.string :name
      t.integer :age
      t.string :desc
      t.string :email
      t.timestamps
    end
  end
end
