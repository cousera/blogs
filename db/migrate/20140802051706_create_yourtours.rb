class CreateYourtours < ActiveRecord::Migration
  def change
    create_table :yourtours do |t|
    	t.string :tour
    	t.string :description
    	t.string :agegroup
    	t.integer :cost
      t.timestamps
    end
  end
end
