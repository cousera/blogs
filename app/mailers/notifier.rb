class Notifier < ActionMailer::Base
  default from: "bshah8288@gmail.com" 

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.send_email.subject
  #
  def send_email(sender_email)
    mail :to => sender_email, :subject => 'email test' 
  end
end