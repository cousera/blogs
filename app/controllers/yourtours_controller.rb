class YourtoursController < ApplicationController
  before_action :set_yourtour, only: [:show, :edit, :update, :destroy]

  # GET /yourtours
  # GET /yourtours.json
  def index
    @yourtours = Yourtour.all
  end

  # GET /yourtours/1
  # GET /yourtours/1.json
  def show
  end

  # GET /yourtours/new
  def new
    @yourtour = Yourtour.new
  end

  # GET /yourtours/1/edit
  def edit
  end

  # POST /yourtours
  # POST /yourtours.json
  def create
    @yourtour = Yourtour.new(yourtour_params)

    respond_to do |format|
      if @yourtour.save
        format.html { redirect_to @yourtour, notice: 'Yourtour was successfully created.' }
        format.json { render :show, status: :created, location: @yourtour }
      else
        format.html { render :new }
        format.json { render json: @yourtour.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /yourtours/1
  # PATCH/PUT /yourtours/1.json
  def update
    respond_to do |format|
      if @yourtour.update(yourtour_params)
        format.html { redirect_to @yourtour, notice: 'Yourtour was successfully updated.' }
        format.json { render :show, status: :ok, location: @yourtour }
      else
        format.html { render :edit }
        format.json { render json: @yourtour.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /yourtours/1
  # DELETE /yourtours/1.json
  def destroy
    @yourtour.destroy
    respond_to do |format|
      format.html { redirect_to yourtours_url, notice: 'Yourtour was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_yourtour
      @yourtour = Yourtour.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def yourtour_params
      params.require(:yourtour).permit(:tour, :description, :agegroup, :cost)
    end
end
