class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def check_login
  	unless session[:id]
  		flash[:error] = "Please Login First"
  		redirect_to(:controller=>'articles', :action=>'index')
  	end
  end
end