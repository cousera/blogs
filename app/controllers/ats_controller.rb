class AtsController < ApplicationController
  before_action :set_at, only: [:show, :edit, :update, :destroy]

  # GET /ats
  # GET /ats.json
  def index
    @ats = At.all
  end

  # GET /ats/1
  # GET /ats/1.json
  def show
  end

  # GET /ats/new
  def new
    @at = At.new
  end

  # GET /ats/1/edit
  def edit
  end

  # POST /ats
  # POST /ats.json
  def create
    @at = At.new(at_params)

    respond_to do |format|
      if @at.save
        format.html { redirect_to @at, notice: 'At was successfully created.' }
        format.json { render :show, status: :created, location: @at }
      else
        format.html { render :new }
        format.json { render json: @at.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ats/1
  # PATCH/PUT /ats/1.json
  def update
    respond_to do |format|
      if @at.update(at_params)
        format.html { redirect_to @at, notice: 'At was successfully updated.' }
        format.json { render :show, status: :ok, location: @at }
      else
        format.html { render :edit }
        format.json { render json: @at.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ats/1
  # DELETE /ats/1.json
  def destroy
    @at.destroy
    respond_to do |format|
      format.html { redirect_to ats_url, notice: 'At was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_at
      @at = At.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def at_params
      params.require(:at).permit(:string, :string)
    end
end
