class ArticlesController < ApplicationController
	before_filter  :check_login , :except=>[:index, :authenticate, :contactus, :contact]
	def index

end

	def authenticate
		em=params[:email]
		ps=params[:password]
		if em.blank?
			flash[:error]="email required"
			redirect_to :action=>"index"
		else
			valid_user=Article.where(:email=>em, :password=>ps).first
			
			if valid_user
				session[:id]=valid_user.id
				redirect_to :action=> "home"
			else
				flash[:error]="invalid username or password"
				redirect_to :action=>"index"
			end
		end
	end

	def home
		id = session[:id]
		@art = Article.find(id)
	end

	def logout
		session[:id] = nil
		flash[:error]="Successfully Logged Out"
				redirect_to :action=>"index"
	end

	def send_mail_to_friend
		Notifier.send_email(params[:name], params[:email].deliver)
		flash[:notice]='mail send'
		redirect_to :action => 'index'
	end


  def contact

  end

  def contactus
    Notifier.send_email(params[:email]).deliver
    flash[:notice] = "mail sent successfully"
    redirect_to :action => "index"
  end

protected
def set_i18n_locale_from_params
if params[:locale]
if I18n.available_locales.include?(params[:locale].to_sym)
I18n.locale = params[:locale]
else
flash.now[:notice] =
"#{params[:locale]} translation not available"
logger.error flash.now[:notice]
end
end
end
def default_url_options
{ :locale => I18n.locale }
end
end

