class Article < ActiveRecord::Base
	validates_presence_of :email
	validates_presence_of :password
	has_one :profile

end
