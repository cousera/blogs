json.array!(@ats) do |at|
  json.extract! at, :id, :string, :string
  json.url at_url(at, format: :json)
end
