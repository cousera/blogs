json.array!(@yourtours) do |yourtour|
  json.extract! yourtour, :id, :tour, :description, :agegroup, :cost
  json.url yourtour_url(yourtour, format: :json)
end
