require 'test_helper'

class YourtoursControllerTest < ActionController::TestCase
  setup do
    @yourtour = yourtours(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:yourtours)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create yourtour" do
    assert_difference('Yourtour.count') do
      post :create, yourtour: { agegroup: @yourtour.agegroup, cost: @yourtour.cost, description: @yourtour.description, tour: @yourtour.tour }
    end

    assert_redirected_to yourtour_path(assigns(:yourtour))
  end

  test "should show yourtour" do
    get :show, id: @yourtour
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @yourtour
    assert_response :success
  end

  test "should update yourtour" do
    patch :update, id: @yourtour, yourtour: { agegroup: @yourtour.agegroup, cost: @yourtour.cost, description: @yourtour.description, tour: @yourtour.tour }
    assert_redirected_to yourtour_path(assigns(:yourtour))
  end

  test "should destroy yourtour" do
    assert_difference('Yourtour.count', -1) do
      delete :destroy, id: @yourtour
    end

    assert_redirected_to yourtours_path
  end
end
