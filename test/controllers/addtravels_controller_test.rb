require 'test_helper'

class AddtravelsControllerTest < ActionController::TestCase
  setup do
    @addtravel = addtravels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:addtravels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create addtravel" do
    assert_difference('Addtravel.count') do
      post :create, addtravel: { email: @addtravel.email, firstname: @addtravel.firstname, lastname: @addtravel.lastname, tourdesc: @addtravel.tourdesc }
    end

    assert_redirected_to addtravel_path(assigns(:addtravel))
  end

  test "should show addtravel" do
    get :show, id: @addtravel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @addtravel
    assert_response :success
  end

  test "should update addtravel" do
    patch :update, id: @addtravel, addtravel: { email: @addtravel.email, firstname: @addtravel.firstname, lastname: @addtravel.lastname, tourdesc: @addtravel.tourdesc }
    assert_redirected_to addtravel_path(assigns(:addtravel))
  end

  test "should destroy addtravel" do
    assert_difference('Addtravel.count', -1) do
      delete :destroy, id: @addtravel
    end

    assert_redirected_to addtravels_path
  end
end
