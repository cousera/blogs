require 'test_helper'

class AtsControllerTest < ActionController::TestCase
  setup do
    @at = ats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create at" do
    assert_difference('At.count') do
      post :create, at: { string: @at.string, string: @at.string }
    end

    assert_redirected_to at_path(assigns(:at))
  end

  test "should show at" do
    get :show, id: @at
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @at
    assert_response :success
  end

  test "should update at" do
    patch :update, id: @at, at: { string: @at.string, string: @at.string }
    assert_redirected_to at_path(assigns(:at))
  end

  test "should destroy at" do
    assert_difference('At.count', -1) do
      delete :destroy, id: @at
    end

    assert_redirected_to ats_path
  end
end
